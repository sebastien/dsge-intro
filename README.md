# Introduction à la modélisation DSGE

Le fichier PDF princial est `dsge-intro.pdf`.

Il peut être obtenu à partir du fichier source `dsge-intro.tex`, à compiler
avec une distribution récente de LaTeX avec Beamer.

Le fichier source est en mode « handout », ce qui est plus adapté à la
distribution d’un PDF sur le web. Pour obtenir des diapositives adaptées à la
présentation, incluant des notes pour le présentateur sur le 2nd écran, il faut
enveler `[handout]` sur la première ligne du fichier.

Le fichier Dynare `basicnk.mod` permet de recréer les fonctions
d’impulsion-réponse du modèle néo-keynésien élementaire qui est présenté.

Les fichiers `inflation-historical.pdf` et `output-historical.pdf` sont issus
de Smets et Wouters (2003).

Le reste du matériel est Copyright © 2015 Sébastien Villemot, et distribué
sous licence Creative Commons Attribution-ShareAlike 4.0.
