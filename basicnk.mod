var y i pi a n nu;
varexo eps_a eps_i;
parameters sigma beta theta epsilon alpha phi phi_pi phi_y rho_a kappa rho lambda zeta rho_nu;

// Structural parameters
sigma = 1;        // Relative risk aversion
beta = 0.99;      // Discount factor
theta = 2/3;      // Price duration
epsilon = 6;      // Elasticity of substitution between goods
alpha = 1/3;      // Capital share
phi = 1;          // Frisch elasticity of labor supply
phi_pi = 1.5;     // Monetary stance on inflation
phi_y = 0.5/4;    // Monetary stance on output
rho_a = 0.9;      // Autocorrelation of technology shock
rho_nu = 0.5;     // Autocorrelation of monetary policy shock

// Derived parameters
rho = -log(beta);
lambda = (1-theta)*(1-beta*theta)*(1-alpha)/theta/(1-alpha+alpha*epsilon);
kappa = lambda*(sigma+(phi+alpha)/(1-alpha));
zeta = lambda*(1+phi)/(1-alpha);

model;
y = y(+1) - 1/sigma*(i-pi(+1)-rho);
pi = beta*pi(+1)+kappa*y-zeta*a;
i = rho + phi_pi*pi + phi_y*y + nu;
a = rho_a*a(-1)+eps_a;
y = a + (1-alpha)*n;
nu = rho_nu*nu(-1) + eps_i;
end;

shocks;
var eps_a; stderr 1;
var eps_i; stderr 0.025;
end;

steady;

stoch_simul(order=1, irf=12) y pi i n;
